import unittest
from driver_class import *


class DriverTests(unittest.TestCase):

	def setUp(self):
		self.func = Driver()

	def test_first_is_evaluated_as_getter(self):
	    with self.assertRaises(AttributeError):
	        self.func.first

	def test_first_is_evaluated_as_setter(self):
		raised = False
		try:
			self.func.first = "Matthew"
		except:
			raised = True
		self.assertFalse(raised, 'Exception raised')

	def test_last_is_evaluated_as_setter(self):
		raised = False
		try:
			self.func.last = "Mitchell"
		except:
			raised = True
		self.assertFalse(raised, 'Exception raised')

	def test_last_is_evaluated_as_getter(self):
		self.func.last = "Mitchell"
		self.assertEqual(self.func.last, "Mitchell")
	
	def test_last_as_getter_with_default_parameter(self):
		self.assertEqual(self.func.last, None)

	def test_if_greet_passenger_greets(self):
		self.func.first = "Magy"
		self.func.last = "Mont"
		self.assertEqual(self.func.greet_passenger(), "Hello! I'll be your driver today. My name is MAGY MONT")

if __name__=="__main__":
    unittest.main()